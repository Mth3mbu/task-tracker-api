package com.tasktracker.api.tasktracker.exceptions;

public class TaskTrackerException extends RuntimeException{
        private String message;

        public TaskTrackerException (String message) {
            this.message = message;
        }

        public String getMessage(){
            return  message;
        }
}
