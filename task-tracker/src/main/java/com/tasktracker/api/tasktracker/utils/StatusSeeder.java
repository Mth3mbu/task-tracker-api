package com.tasktracker.api.tasktracker.utils;

import com.tasktracker.api.tasktracker.dtos.StatusDto;
import com.tasktracker.api.tasktracker.models.Status;
import com.tasktracker.api.tasktracker.repositories.StatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class StatusSeeder implements CommandLineRunner {
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void run(String... args) {
        createStatuses();
    }

    public void createStatuses(){
        var statuses = new ArrayList<StatusDto>();

        statuses.add(new StatusDto(1,"Pending"));
        statuses.add(new StatusDto(2,"In Progress"));
        statuses.add(new StatusDto(3,"Resolved"));
        statuses.add(new StatusDto(4,"Done"));

       var records = statuses.stream()
               .map(status-> modelMapper.map(status, Status.class))
               .collect(Collectors.toList());

      var status=  statusRepository.findById(1).isEmpty();
      if(status) statusRepository.saveAll(records);
    }
}
