package com.tasktracker.api.tasktracker.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class CreateTaskRequest {
    @NotEmpty(message = "Please provide a task name")
    private String name;

    @NotEmpty(message = "Please provide a task description")
    private String description;

    @Min(value = 1, message = "Status id must be between 1 and 4")
    @Max(value = 4, message = "Status id must be between 1 and 4")
    @JsonProperty("status_id")

    private int statusId;
    @JsonProperty("date_time")
    private Date dateTime;
}
