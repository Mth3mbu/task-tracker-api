package com.tasktracker.api.tasktracker.services;

import com.tasktracker.api.tasktracker.dtos.UserDto;
import com.tasktracker.api.tasktracker.exceptions.ResourceNotFoundException;
import com.tasktracker.api.tasktracker.exceptions.TaskTrackerException;
import com.tasktracker.api.tasktracker.models.User;
import com.tasktracker.api.tasktracker.repositories.UserRepository;
import com.tasktracker.api.tasktracker.requests.CreateUserRequest;
import com.tasktracker.api.tasktracker.requests.UpdateUserRequest;
import com.tasktracker.api.tasktracker.response.UserResponse;
import com.tasktracker.api.tasktracker.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;

@Service
public class AppUserService implements UserService {
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    public AppUserService(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDto createUser(CreateUserRequest request) {
        var existingUser = userRepository.findByUsername(request.getUsername());

        if(existingUser.isPresent()){
            throw new TaskTrackerException(String.format("%s is already taken", request.getUsername()));
        }

        var user = userRepository.save(modelMapper.map(request, User.class));
        return modelMapper.map(user,UserDto.class);
    }

    @Override
    public UserDto updateUser(UpdateUserRequest request, long userId) {
        var existingUser = findUserById(userId);

        existingUser.setFirstName(request.getFirstName());
        existingUser.setLastName(request.getLastName());

        var updateUser = userRepository.save(existingUser);

        return modelMapper.map(updateUser,UserDto.class);
    }

    @Override
    public UserDto getUserById(long userId) {
        return modelMapper.map(findUserById(userId), UserDto.class);
    }

    @Override
    public Page<User> getAllUsers(int pageNo, int pageSize) {
        var pageRequest = PageRequest.of(pageNo,pageSize);
        return userRepository.findAll(pageRequest);
    }

    private User findUserById(long userId){
      return userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("User","Id",String.format("%s",userId)));
    }
}
