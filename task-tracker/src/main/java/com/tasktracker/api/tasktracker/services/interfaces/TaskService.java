package com.tasktracker.api.tasktracker.services.interfaces;

import com.tasktracker.api.tasktracker.dtos.TaskDto;
import com.tasktracker.api.tasktracker.requests.CreateTaskRequest;
import com.tasktracker.api.tasktracker.requests.UpdateTaskRequest;
import com.tasktracker.api.tasktracker.response.DeleteTaskResponse;

import java.util.List;

public interface TaskService {
    TaskDto createTask(CreateTaskRequest request, long userId);
    TaskDto updateTask(long userId,long taskId,UpdateTaskRequest request);
    DeleteTaskResponse deleteTask(long taskId, long userId);
    List<TaskDto> getTaskByUserId(long userId);
    TaskDto getTaskById(long taskId);
}
