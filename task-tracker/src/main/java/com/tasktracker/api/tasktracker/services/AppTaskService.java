package com.tasktracker.api.tasktracker.services;

import com.tasktracker.api.tasktracker.dtos.TaskDto;
import com.tasktracker.api.tasktracker.exceptions.ResourceNotFoundException;
import com.tasktracker.api.tasktracker.models.Task;
import com.tasktracker.api.tasktracker.models.User;
import com.tasktracker.api.tasktracker.repositories.TaskRepository;
import com.tasktracker.api.tasktracker.repositories.UserRepository;
import com.tasktracker.api.tasktracker.requests.CreateTaskRequest;
import com.tasktracker.api.tasktracker.requests.UpdateTaskRequest;
import com.tasktracker.api.tasktracker.response.DeleteTaskResponse;
import com.tasktracker.api.tasktracker.services.interfaces.TaskService;
import com.tasktracker.api.tasktracker.services.interfaces.TaskStatusService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppTaskService implements TaskService {
    private UserRepository userRepository;
    private TaskRepository taskRepository;
    private ModelMapper modelMapper;
    private TaskStatusService taskStatusService;

    public AppTaskService(UserRepository userRepository, TaskRepository taskRepository, ModelMapper modelMapper, TaskStatusService taskStatusService) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.modelMapper = modelMapper;
        this.taskStatusService = taskStatusService;
    }

    @Override
    public TaskDto createTask(CreateTaskRequest request, long userId) {
        var user = findUserById(userId);

        var newTask = new Task();
        newTask.setDateTime(request.getDateTime());
        newTask.setDescription(request.getDescription());
        newTask.setName(request.getName());
        newTask.setUser(user);

        var task = taskRepository.save(newTask);
        var taskStatus= taskStatusService.createTaskStatus(request.getStatusId(),task);

        return taskStatus.getTask();
    }

    @Override
    public TaskDto updateTask(long userId,long taskId,UpdateTaskRequest request) {
        var existingTask= findTaskById(taskId);
        existingTask.setName(request.getName());

        var task = taskRepository.save(existingTask);
        var response = taskStatusService.updateTaskStatus(request.getStatusId(),task);

        return response.getTask();
    }

    @Override
    public DeleteTaskResponse deleteTask(long taskId, long userId) {
        taskRepository.deleteById(taskId);

        return new DeleteTaskResponse(String.format("Task with id %s deleted successfully",taskId));
    }

    @Override
    public List<TaskDto> getTaskByUserId(long userId) {
        return taskRepository.findAllByUserId(userId).stream().map(task ->
                        new TaskDto(task.getId(),
                        task.getName(),
                        task.getTaskStatus().getStatus().getName(),
                        task.getDescription(),
                        task.getDateTime()))
                .collect(Collectors.toList());
    }

    @Override
    public TaskDto getTaskById(long taskId) {
        var task = findTaskById(taskId);
        var taskDto = modelMapper.map(task,TaskDto.class);
        taskDto.setStatus(task.getTaskStatus().getStatus().getName());

        return taskDto;
    }

    private User findUserById(long userId){
        return userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("User","Id",String.format("%s",userId)));
    }

    private Task findTaskById(long taskId){
        return taskRepository.findById(taskId)
                .orElseThrow(()-> new ResourceNotFoundException("Task","Id",String.format("%s",taskId)));
    }
}
