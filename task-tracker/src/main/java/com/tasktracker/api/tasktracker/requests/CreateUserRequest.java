package com.tasktracker.api.tasktracker.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.validation.constraints.NotEmpty;

@Data
public class CreateUserRequest {
    @NotEmpty(message = "Please provide a username")
    private String username;
    @NotEmpty(message = "Please provide a first name")
    @JsonProperty("first_name")
    private String firstName;
    @NotEmpty(message = "Please provide a last name")
    @JsonProperty("last_name")
    private String lastName;
}
