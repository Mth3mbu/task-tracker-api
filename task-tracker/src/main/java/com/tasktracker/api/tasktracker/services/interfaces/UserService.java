package com.tasktracker.api.tasktracker.services.interfaces;

import com.tasktracker.api.tasktracker.dtos.UserDto;
import com.tasktracker.api.tasktracker.models.User;
import com.tasktracker.api.tasktracker.requests.CreateUserRequest;
import com.tasktracker.api.tasktracker.requests.UpdateUserRequest;
import org.springframework.data.domain.Page;

public interface UserService {
    UserDto createUser(CreateUserRequest request);
    UserDto updateUser(UpdateUserRequest request, long userId);
    UserDto getUserById(long userId);
    Page<User> getAllUsers(int pageNo, int pageSize);
}
