package com.tasktracker.api.tasktracker.response;

import com.tasktracker.api.tasktracker.dtos.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {
    private List<UserDto> content;
    private int pageNo;
    private int pageSize;
    private long totalRecords;
    private int totalPages;
    private boolean isLast;
}
