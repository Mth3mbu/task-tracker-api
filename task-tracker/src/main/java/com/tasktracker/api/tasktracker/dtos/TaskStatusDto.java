package com.tasktracker.api.tasktracker.dtos;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskStatusDto {
    private TaskDto task;
    private StatusDto status;
}
