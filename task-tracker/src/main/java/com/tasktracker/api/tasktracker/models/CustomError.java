package com.tasktracker.api.tasktracker.models;

import java.util.Date;

public class CustomError {
    private Date timestamp;
    private String message;
    private String url;

    public CustomError (Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.url = details;
    }

    public String getUrl() {
        return url;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
