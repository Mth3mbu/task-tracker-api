package com.tasktracker.api.tasktracker.repositories;

import com.tasktracker.api.tasktracker.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Integer> {
}
