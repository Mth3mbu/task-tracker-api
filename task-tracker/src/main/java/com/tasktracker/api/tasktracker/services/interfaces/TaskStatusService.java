package com.tasktracker.api.tasktracker.services.interfaces;

import com.tasktracker.api.tasktracker.dtos.TaskStatusDto;
import com.tasktracker.api.tasktracker.models.Task;

public interface TaskStatusService {
    TaskStatusDto createTaskStatus(int statusId, Task task);
    TaskStatusDto updateTaskStatus(int statusId, Task task);
}
