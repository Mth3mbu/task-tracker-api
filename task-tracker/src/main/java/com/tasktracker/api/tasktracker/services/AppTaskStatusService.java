package com.tasktracker.api.tasktracker.services;

import com.tasktracker.api.tasktracker.dtos.StatusDto;
import com.tasktracker.api.tasktracker.dtos.TaskDto;
import com.tasktracker.api.tasktracker.dtos.TaskStatusDto;
import com.tasktracker.api.tasktracker.exceptions.ResourceNotFoundException;
import com.tasktracker.api.tasktracker.models.Status;
import com.tasktracker.api.tasktracker.models.Task;
import com.tasktracker.api.tasktracker.models.TaskStatus;
import com.tasktracker.api.tasktracker.repositories.StatusRepository;
import com.tasktracker.api.tasktracker.repositories.TaskStatusRepository;
import com.tasktracker.api.tasktracker.services.interfaces.TaskStatusService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class AppTaskStatusService implements TaskStatusService {
    private ModelMapper modelMapper;
    private TaskStatusRepository taskStatusRepository;
    private StatusRepository statusRepository;

    public AppTaskStatusService(ModelMapper modelMapper, TaskStatusRepository taskStatusRepository, StatusRepository statusRepository) {
        this.modelMapper = modelMapper;
        this.taskStatusRepository = taskStatusRepository;
        this.statusRepository = statusRepository;
    }

    @Override
    public TaskStatusDto createTaskStatus(int statusId,Task task) {
        var status = getStatus(statusId);
        var taskStatus = createTaskStatus(task, status);
        var taskDto = mapTaskEntityToDto(task);

        taskDto.setStatus(status.getName());
        taskStatusRepository.save(taskStatus);

        return new TaskStatusDto(taskDto,mapStatusEntityToDto(status));
    }

    @Override
    public TaskStatusDto updateTaskStatus(int statusId, Task task) {
        var status = getStatus(statusId);
        var taskStatus = createTaskStatus(task.getId());

        taskStatus.setStatus(status);
        taskStatusRepository.save(taskStatus);

        var taskDto = mapTaskEntityToDto(task);
        taskDto.setStatus(status.getName());

        return new TaskStatusDto(taskDto,mapStatusEntityToDto(status));
    }

    private TaskStatus createTaskStatus(long taskId) {
        return taskStatusRepository.findByTaskId(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Status", "name", String.format("%s", taskId)));
    }

    private Status getStatus(int statusId) {
        return statusRepository.findById(statusId)
                .orElseThrow(() -> new ResourceNotFoundException("Status", "name", String.format("%s", statusId)));
    }

    private TaskStatus createTaskStatus(Task task, Status status) {
        var taskStatus = new TaskStatus();
        taskStatus.setStatus(status);
        taskStatus.setTask(task);

        return taskStatus;
    }

    private StatusDto mapStatusEntityToDto(Status status) {
        return modelMapper.map(status, StatusDto.class);
    }

    private TaskDto mapTaskEntityToDto(Task task) {
        return modelMapper.map(task, TaskDto.class);
    }
}
