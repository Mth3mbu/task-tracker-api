package com.tasktracker.api.tasktracker.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UpdateTaskRequest {
    @NotEmpty(message = "Please provide a task name")
    private String name;
    @NotNull
    @Min(value = 1, message = "Status id must be between 1 and 4")
    @Max(value = 4, message = "Status id must be between 1 and 4")
    @JsonProperty("status_id")
    private int statusId;
}
