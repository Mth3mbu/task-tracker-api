package com.tasktracker.api.tasktracker.repositories;

import com.tasktracker.api.tasktracker.models.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskStatusRepository extends JpaRepository<TaskStatus, Long> {
    Optional<TaskStatus> findByTaskId(Long task_id);
}
