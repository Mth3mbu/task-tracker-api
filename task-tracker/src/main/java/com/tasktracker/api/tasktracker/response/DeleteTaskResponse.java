package com.tasktracker.api.tasktracker.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeleteTaskResponse {
    private String response;
}
