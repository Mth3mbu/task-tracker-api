package com.tasktracker.api.tasktracker.controllers;

import com.tasktracker.api.tasktracker.dtos.TaskDto;
import com.tasktracker.api.tasktracker.requests.CreateTaskRequest;
import com.tasktracker.api.tasktracker.requests.UpdateTaskRequest;
import com.tasktracker.api.tasktracker.response.DeleteTaskResponse;
import com.tasktracker.api.tasktracker.services.interfaces.TaskService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class TaskController {
    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/{userId}/tasks")
    public ResponseEntity<TaskDto> createTask(@PathVariable long userId,
                                               @Valid @RequestBody CreateTaskRequest request){
        return new ResponseEntity<>(taskService.createTask(request,userId), HttpStatus.CREATED);
    }

    @PutMapping("/{userId}/tasks/{taskId}")
    public ResponseEntity<TaskDto> updateTask(@PathVariable long userId,
                                              @PathVariable long taskId,
                                              @Valid @RequestBody UpdateTaskRequest request){
        return new ResponseEntity<>(taskService.updateTask(userId,taskId,request), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}/tasks")
    public List<TaskDto> getTaskByUserId(@PathVariable long userId){
        return taskService.getTaskByUserId(userId);
    }

    @GetMapping("/{userId}/tasks/{taskId}")
    public TaskDto getTask(@PathVariable long userId,
                           @PathVariable long taskId){
        return taskService.getTaskById(taskId);
    }

    @DeleteMapping("/{userId}/tasks/{taskId}")
    public DeleteTaskResponse deleteTask(@PathVariable long userId,
                                         @PathVariable long taskId){
        return taskService.deleteTask(taskId,userId);
    }
}
