package com.tasktracker.api.tasktracker.controllers;

import com.tasktracker.api.tasktracker.dtos.UserDto;
import com.tasktracker.api.tasktracker.models.User;
import com.tasktracker.api.tasktracker.requests.CreateUserRequest;
import com.tasktracker.api.tasktracker.requests.UpdateUserRequest;
import com.tasktracker.api.tasktracker.services.interfaces.UserService;
import com.tasktracker.api.tasktracker.utils.AppConstants;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody CreateUserRequest request){
        return new ResponseEntity<>(userService.createUser(request), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@Valid @RequestBody UpdateUserRequest request,
                                              @PathVariable long id){
        return new ResponseEntity<>(userService.updateUser(request,id), HttpStatus.CREATED);
    }

    @GetMapping
    public Page<User> getUsers(@RequestParam(name = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
                               @RequestParam(name = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize){
        return userService.getAllUsers(pageNo,pageSize);
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable long id){
        return userService.getUserById(id);
    }
}
