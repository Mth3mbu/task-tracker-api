package com.tasktracker.api.tasktracker.repositories;

import com.tasktracker.api.tasktracker.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Long> {
    List<Task> findAllByUserId(long user_id);
}
