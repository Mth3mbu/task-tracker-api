package com.tasktracker.api.tasktracker.utils;

public class AppConstants {
    public static final  String DEFAULT_PAGE_NUMBER ="0";
    public static final  String DEFAULT_PAGE_SIZE ="10";
    public static final  String AUTHORIZATION_HEADER ="Authorization";
}
