package com.tasktracker.api.tasktracker.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    private long id;
    private String name;
    private String status;
    private String description;
    @JsonProperty("date_time")
    private Date dateTime;
}
